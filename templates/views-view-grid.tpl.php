<?php
/**
 * @file views-view-grid.tpl.php
 * Simple view template to display a rows in a grid.
 *
 * Based on a hack by Jen Simmons to render views grids as unordered lists.
 *
 * @see https://gist.github.com/2295479
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul class="<?php print $class; ?> clearfix"<?php print $attributes; ?>>
  <?php foreach ($rows as $row_number => $columns): ?>
    <?php foreach ($columns as $column_number => $item): ?>
      <?php if(!empty($item)): ?>
        <li class="<?php print $column_classes[$row_number][$column_number]; ?>">
          <?php print $item; ?>
        </li>
      <?php endif; ?>
    <?php endforeach; ?>
  <?php endforeach; ?>
</ul>
