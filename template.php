<?php

/**
 * @file
 * This file provides theme override functions for the theatre theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function theatre_preprocess_html(&$variables) {

  // Add viewport metatag for mobile devices.
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Set up IE meta tag to force IE rendering mode.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' =>  'IE=edge,chrome=1',
    ),
  );
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  // Additional classes for body element.
  if (user_access('administer site configuration')) {
    $variables['classes_array'][] = 'admin';
  }

  // Add classes for website section.
  if (!$variables['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    if (arg(0) == 'node' && arg(1) == 'add') {
      $variables['classes_array'][] = 'section-node-add';
    }
    elseif (arg(0)== 'node' && is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      $variables['classes_array'][] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
    else {
      list($section, ) = explode('/', $path, 2);
      $variables['classes_array'][] = theatre_id_safe('section-' . $section);
    }
  }
}

/**
 * Implements hook_template_preprocess_node().
 *
 * Preprocess variables for node.tpl.php.
 */
function theatre_preprocess_node(&$variables) {
  global $user;
  if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
    $variables['classes_array'][] = 'node-mine';
  }
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function theatre_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}

/**
 * Implements hook_template_preprocess_field().
 */
function theatre_preprocess_field(&$variables) {
  if ($variables['element']['#field_name'] == 'field_pic_article') {
    $variables['classes_array'][] = 'photo-border';
  }

}
